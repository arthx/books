-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema bookdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bookdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bookdb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `bookdb` ;

-- -----------------------------------------------------
-- Table `bookdb`.`file`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`file` (
  `id_file` BIGINT NOT NULL AUTO_INCREMENT,
  `pathfile` VARCHAR(155) NULL,
  PRIMARY KEY (`id_file`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`settings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`settings` (
  `id_settings` BIGINT NOT NULL AUTO_INCREMENT,
  `location_show` TINYINT NULL,
  PRIMARY KEY (`id_settings`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`type` (
  `id_type` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) NULL,
  `shortname` VARCHAR(8) NULL,
  PRIMARY KEY (`id_type`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`district`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`district` (
  `id_district` VARCHAR(13) NOT NULL,
  `name` VARCHAR(45) NULL,
  `zip` VARCHAR(45) NULL,
  `okato` VARCHAR(45) NULL,
  `type_id` INT NOT NULL,
  PRIMARY KEY (`id_district`),
  INDEX `fk_district_type1_idx` (`type_id` ASC),
  CONSTRAINT `fk_district_type1`
    FOREIGN KEY (`type_id`)
    REFERENCES `bookdb`.`type` (`id_type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`street`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`street` (
  `id_street` VARCHAR(17) NOT NULL,
  `name` VARCHAR(45) NULL,
  `zip` VARCHAR(45) NULL,
  `okato` VARCHAR(45) NULL,
  `type_id` INT NOT NULL,
  PRIMARY KEY (`id_street`),
  INDEX `fk_street_type1_idx` (`type_id` ASC),
  CONSTRAINT `fk_street_type1`
    FOREIGN KEY (`type_id`)
    REFERENCES `bookdb`.`type` (`id_type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`house`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`house` (
  `id_house` BIGINT NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(15) NULL,
  PRIMARY KEY (`id_house`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`region`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`region` (
  `id_region` VARCHAR(13) NOT NULL,
  `name` VARCHAR(45) NULL,
  `zip` VARCHAR(45) NULL,
  `okato` VARCHAR(45) NULL,
  `type_id` INT NOT NULL,
  PRIMARY KEY (`id_region`),
  INDEX `fk_region_type1_idx` (`type_id` ASC),
  CONSTRAINT `fk_region_type1`
    FOREIGN KEY (`type_id`)
    REFERENCES `bookdb`.`type` (`id_type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`city` (
  `id_city` VARCHAR(13) NOT NULL,
  `name` VARCHAR(45) NULL,
  `zip` VARCHAR(45) NULL,
  `okato` VARCHAR(45) NULL,
  `type_id` INT NOT NULL,
  PRIMARY KEY (`id_city`),
  INDEX `fk_city_type1_idx` (`type_id` ASC),
  CONSTRAINT `fk_city_type1`
    FOREIGN KEY (`type_id`)
    REFERENCES `bookdb`.`type` (`id_type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`location`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`location` (
  `id_location` BIGINT NOT NULL AUTO_INCREMENT,
  `district_id` VARCHAR(13) NULL,
  `street_id` VARCHAR(17) NOT NULL,
  `house_id` BIGINT NULL,
  `region_id` VARCHAR(13) NULL,
  `city_id` VARCHAR(13) NOT NULL,
  PRIMARY KEY (`id_location`),
  INDEX `fk_location_district1_idx` (`district_id` ASC),
  INDEX `fk_location_street1_idx` (`street_id` ASC),
  INDEX `fk_location_house1_idx` (`house_id` ASC),
  INDEX `fk_location_region1_idx` (`region_id` ASC),
  INDEX `fk_location_city1_idx` (`city_id` ASC),
  CONSTRAINT `fk_location_district1`
    FOREIGN KEY (`district_id`)
    REFERENCES `bookdb`.`district` (`id_district`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_location_street1`
    FOREIGN KEY (`street_id`)
    REFERENCES `bookdb`.`street` (`id_street`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_location_house1`
    FOREIGN KEY (`house_id`)
    REFERENCES `bookdb`.`house` (`id_house`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_location_region1`
    FOREIGN KEY (`region_id`)
    REFERENCES `bookdb`.`region` (`id_region`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_location_city1`
    FOREIGN KEY (`city_id`)
    REFERENCES `bookdb`.`city` (`id_city`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`user` (
  `id_user` BIGINT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NULL,
  `password` VARCHAR(255) NULL,
  `lastname` VARCHAR(45) NULL,
  `firstname` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `phone` VARCHAR(45) NULL,
  `sex` TINYINT NULL,
  `profile_photo` BIGINT NOT NULL,
  `scope` DOUBLE NULL,
  `settings` BIGINT NOT NULL,
  `location_id` BIGINT NOT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC),
  INDEX `fk_user_file1_idx` (`profile_photo` ASC),
  INDEX `fk_user_settings1_idx` (`settings` ASC),
  INDEX `fk_user_location1_idx` (`location_id` ASC),
  CONSTRAINT `fk_user_file1`
    FOREIGN KEY (`profile_photo`)
    REFERENCES `bookdb`.`file` (`id_file`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_settings1`
    FOREIGN KEY (`settings`)
    REFERENCES `bookdb`.`settings` (`id_settings`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_location1`
    FOREIGN KEY (`location_id`)
    REFERENCES `bookdb`.`location` (`id_location`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`role` (
  `id_role` BIGINT NOT NULL AUTO_INCREMENT,
  `rolename` VARCHAR(45) NULL,
  PRIMARY KEY (`id_role`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`user_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`user_role` (
  `user_id` BIGINT NOT NULL,
  `role_id` BIGINT NOT NULL,
  INDEX `fk_user_role_user_idx` (`user_id` ASC),
  INDEX `fk_user_role_role1_idx` (`role_id` ASC),
  CONSTRAINT `fk_user_role_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `bookdb`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_role_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `bookdb`.`role` (`id_role`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`book`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`book` (
  `id_book` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `author` VARCHAR(45) NULL,
  `publishing` VARCHAR(45) NULL,
  `release_date` DATE NULL,
  `version` VARCHAR(45) NULL,
  `score` DOUBLE NULL,
  `image` BIGINT NOT NULL,
  PRIMARY KEY (`id_book`),
  INDEX `fk_book_file1_idx` (`image` ASC),
  CONSTRAINT `fk_book_file1`
    FOREIGN KEY (`image`)
    REFERENCES `bookdb`.`file` (`id_file`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`trade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`trade` (
  `id_trade` BIGINT NOT NULL AUTO_INCREMENT,
  `trade_date` DATE NULL,
  `planed_date` DATE NULL,
  `reverse_date` DATE NULL,
  `location` BIGINT NULL,
  `status` VARCHAR(35) NULL,
  `scope` DOUBLE NULL,
  `comment` TEXT NULL,
  `user` BIGINT NOT NULL,
  `book_id` BIGINT NOT NULL,
  PRIMARY KEY (`id_trade`),
  INDEX `fk_trade_user1_idx` (`user` ASC),
  INDEX `fk_trade_book1_idx` (`book_id` ASC),
  CONSTRAINT `fk_trade_user1`
    FOREIGN KEY (`user`)
    REFERENCES `bookdb`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_trade_book1`
    FOREIGN KEY (`book_id`)
    REFERENCES `bookdb`.`book` (`id_book`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`tag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`tag` (
  `id_tag` BIGINT NOT NULL,
  `tag` VARCHAR(45) NULL,
  PRIMARY KEY (`id_tag`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`tag_book`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`tag_book` (
  `tag_id` BIGINT NOT NULL,
  `book_id` BIGINT NOT NULL,
  INDEX `fk_tag_book_tag1_idx` (`tag_id` ASC),
  INDEX `fk_tag_book_book1_idx` (`book_id` ASC),
  CONSTRAINT `fk_tag_book_tag1`
    FOREIGN KEY (`tag_id`)
    REFERENCES `bookdb`.`tag` (`id_tag`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tag_book_book1`
    FOREIGN KEY (`book_id`)
    REFERENCES `bookdb`.`book` (`id_book`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`user_tag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`user_tag` (
  `user_id` BIGINT NOT NULL,
  `tag_id` BIGINT NOT NULL,
  INDEX `fk_user_tag_user1_idx` (`user_id` ASC),
  INDEX `fk_user_tag_tag1_idx` (`tag_id` ASC),
  CONSTRAINT `fk_user_tag_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `bookdb`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_tag_tag1`
    FOREIGN KEY (`tag_id`)
    REFERENCES `bookdb`.`tag` (`id_tag`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`user_book`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`user_book` (
  `user_id` BIGINT NOT NULL,
  `book_id` BIGINT NOT NULL,
  INDEX `fk_user_book_user1_idx` (`user_id` ASC),
  INDEX `fk_user_book_book1_idx` (`book_id` ASC),
  CONSTRAINT `fk_user_book_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `bookdb`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_book_book1`
    FOREIGN KEY (`book_id`)
    REFERENCES `bookdb`.`book` (`id_book`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bookdb`.`wishbook`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bookdb`.`wishbook` (
  `user_id` BIGINT NOT NULL,
  `book_id` BIGINT NOT NULL,
  INDEX `fk_wishbook_user1_idx` (`user_id` ASC),
  INDEX `fk_wishbook_book1_idx` (`book_id` ASC),
  CONSTRAINT `fk_wishbook_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `bookdb`.`user` (`id_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_wishbook_book1`
    FOREIGN KEY (`book_id`)
    REFERENCES `bookdb`.`book` (`id_book`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
