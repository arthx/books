package su.arth.dashboard.provider;

import su.arth.dashboard.domain.*;

import java.util.List;

/**
 * QuickTickets Dashboard backend API.
 */
public interface DataProvider {

    /**
     * @param userName
     * @param password
     * @return Authenticated used.
     */
    User authenticate(String userName, String password);

    User registredUser(User user);

    User updatedUser(User user);

    void addFile(File file,long idUser);

    List<su.arth.dashboard.domain.Book> getBooksByUser();


    void addMyBook(su.arth.dashboard.domain.Book book);

    List<Book> getWishBookByUser();
    void addWishBook(Book book);

    List<Book> getBooksAll();

    Type getType(String name);

    void removeMyBook(Book book);
    void removeWishBook(Book book);
    List<City> getCityList();

    City getCity(String id);
    Region getRegion(String id);
    District getDistrict(String id);
    Street getStreet(String id);
    House getHouse(String name);


    boolean isLogin(String login);



//    Location getMyLocation(User user);




}
