package su.arth.dashboard.provider;

import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Notification;
import su.arth.dashboard.MyUI;
import su.arth.dashboard.domain.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

public class EntityDataProvider implements DataProvider {


    private EntityTransaction tx;
    private static Logger log = Logger.getLogger(EntityDataProvider.class.getName());


    private EntityManager em;
    /**
     * Initialize the data for this application.
     */
    public EntityDataProvider() {

        em = JPAContainerFactory.
                createEntityManagerForPersistenceUnit(MyUI.PERSISTENCE_UNIT);

        tx = em.getTransaction();
    }



    @Override
    public User authenticate(String userName, String password) {




        List<User> userList = em.createQuery("SELECT u FROM User u where u.login=:login").setParameter("login",userName).getResultList();
        User user = null;
       password = passwordEncoder(password);


        if(userList.size()>0){
          user = userList.get(0);
            if(!user.getPassword().equals(password)){
                return null;
            }
        }


        return user;
    }

    @Override
    public User updatedUser(User user) {
         User userNew = null;

          tx.begin();
            userNew = em.find(User.class,user.getIdUser());
             em.persist(user);

            tx.commit();


        return user;
    }

    @Override
    public Type getType(String name){
        Type type = null;

        List resultList = em.createQuery("select t from Type t where t.name=:name").setParameter("name", name).getResultList();
        if(resultList.size()>0){
            type = (Type) resultList.get(0);
        }
        return type;
   }

    @Override
    public City getCity(String id){
        return em.find(City.class,id);
    }

    @Override
    public District getDistrict(String id){
        return em.find(District.class,id);
    }
    @Override
    public Region getRegion(String id){
        return em.find(Region.class,id);
    }

    @Override
    public Street getStreet(String id){
        return em.find(Street.class,id);
    }

    @Override
    public boolean isLogin(String login) {
         if(em.createQuery("select u from User u where u.login=:login").setParameter("login",login).getResultList().size()>0){
             return true;
         }
        return false;
    }


    @Override
    public House getHouse(String number) {
        House house = null;
       List houses = em.createQuery("select h from House h where h.number = :number").setParameter("number",number).getResultList();

        if(houses.size()>0){
            house = (House) houses.get(0);
        }
     return house;
    }


    @Override
    public List<City> getCityList() {
        return em.createQuery("select c from City c").getResultList();
    }






    @Override
    public User registredUser(User user) {
        try {
            user.getRoles()
                  .add(getDefaultRole());
         user.setPassword(passwordEncoder(user.getPassword()));
         user.setProfile_photo(getDefaultProfilePhoto());
         user.setSettings(getDefaultSettings());
         Location location = user.getLocation();
         Street street = location.getStreet();
            City city = location.getCity();
         District district = location.getDistrict();
         House house = location.getHouse();
         Region region = location.getRegion();


         tx.begin();
            em.persist(city);
            if(district!=null) em.persist(district);
             if(region!=null) em.persist(region);
           if(house!=null) em.persist(house);
            em.persist(street);
            em.persist(location);


         em.persist(user);
         tx.commit();
     } catch (Exception e){
            tx.rollback();
            log.info(e.getMessage());
            Notification.show("Ошибка регистрации", Notification.Type.ERROR_MESSAGE);
     }
       return user;
     }

    public Role getDefaultRole(){
       return em.getReference(Role.class,1l);
    }

    public su.arth.dashboard.domain.File getDefaultBookImage(){
        return em.getReference(su.arth.dashboard.domain.File.class,1l);
    }
    private Date getDay(Date time) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(time);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        return cal.getTime();
    }






    @Override
    public void addFile(su.arth.dashboard.domain.File file,long idUser) {


         User user = em.find(User.class,idUser);
          tx.begin();
          em.persist(file);
          user.setProfile_photo(file);
          em.flush();

          tx.commit();



      //  em.refresh(user);

    }


    public String passwordEncoder(String password){
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        md.update(password.getBytes());
        byte byteData[] = md.digest();

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
         return sb.toString();
    }


    public su.arth.dashboard.domain.File getDefaultProfilePhoto(){
        return em.getReference(su.arth.dashboard.domain.File.class,2l);
    }


    private Settings getDefaultSettings(){
        return em.getReference(Settings.class,1l);
    }


    @Override
    public List<su.arth.dashboard.domain.Book> getBooksByUser() {
      //  em = JPAContainerFactory.createEntityManagerForPersistenceUnit(MyUI.PERSISTENCE_UNIT);
     //   tx = em.getTransaction();
        User user = (User) VaadinSession.getCurrent().getAttribute(
                User.class.getName());
        List<su.arth.dashboard.domain.Book> books;

      //  removeMyBook(new Book());
          user = em.find(User.class,user.getIdUser());
        List list = em.createQuery("select b from Book b where b.owner.idUser in (:idUser)").setParameter("idUser", user.getIdUser()).getResultList();

     //   em.close();

        return list;
    }


    public List<Book> getWishBookByUser(){
        User user = (User) VaadinSession.getCurrent().getAttribute(
                User.class.getName());
        List<Book> list = em.createQuery("select b from Book b join b.user user where user.idUser=:idUser").setParameter("idUser",user.getIdUser()).getResultList();
return list;
    }

    @Override
    public void removeMyBook(Book book){

        try {
           User user = (User) VaadinSession.getCurrent().getAttribute(User.class.getName());
         //  book = em.find(Book.class,104L);
            tx.begin();
//            Set<User> users = book.getUser();
//            for(User u : users){
//                u.getMybooks().remove(book);
//                em.merge(u);
            //}

            em.remove(book);

            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
    }




    public void removeWishBook(Book book){
        User user = (User) VaadinSession.getCurrent().getAttribute(User.class.getName());

                   List<Book> wishBook = user.getWishbooks();
        wishBook.remove(book);
        em.flush();


    }

    @Override
    public List<Book> getBooksAll() {
      //  em = JPAContainerFactory.createEntityManagerForPersistenceUnit(MyUI.PERSISTENCE_UNIT);
        List<Book> list =
         em.createQuery("select b from Book b").getResultList();

      //  em.close();
    return list;
    }

    public void addMyBook(su.arth.dashboard.domain.Book book){

      //  em = JPAContainerFactory.createEntityManagerForPersistenceUnit(MyUI.PERSISTENCE_UNIT);
     //   tx = em.getTransaction();
        User user = (User) VaadinSession.getCurrent().getAttribute(
                User.class.getName());
        user = em.find(User.class,user.getIdUser());
        List<su.arth.dashboard.domain.Book> myBookList = user.getMybooks();

        tx.begin();
        book.setOwner(user);
        book.setImage(getDefaultProfilePhoto());
        book.setImage(getDefaultBookImage());
        em.persist(book);
        myBookList.add(book);
       user.setMybooks(myBookList);

        em.flush();
        tx.commit();
//em.close();
    }





    public void addWishBook(Book book){

        User user = (User) VaadinSession.getCurrent().getAttribute(
                User.class.getName());
        tx.begin();
        user = em.find(User.class,user.getIdUser());
        List<su.arth.dashboard.domain.Book> bookList = user.getWishbooks();
        bookList.add(book);
        user.setWishbooks(bookList);
//        em.persist(user);
tx.commit();
    }





}
