package su.arth.dashboard.provider;

import com.vaadin.server.FileResource;
import com.vaadin.ui.*;


import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by arthur on 03.04.16.
 */

public class MyUploader extends CustomComponent implements Upload.SucceededListener,
        Upload.FailedListener,
        Upload.Receiver {

    Panel root;         // Root element for contained components.
    Panel imagePanel;   // Panel that contains the uploaded image.
    File file;         // File to write to.

   public MyUploader() {
        root = new Panel("My Upload Component");
//        setCompositionRoot(root);

        final Upload upload =
                new Upload("Upload the file here", this);
upload.setButtonCaption("Upload Now");

        // Listen for events regarding the success of upload.
        upload.addListener((Upload.SucceededListener) this);
        upload.addListener((Upload.FailedListener) this);
root.setContent(upload);

//        root.addComponent(upload);

       root.setContent(new Label("Выберите файл для загрузки"));

        imagePanel = new Panel("Uploaded image");
        imagePanel.setContent(new Label("No image uploaded yet"));
        root.setContent(imagePanel);
    }

    // Callback method to begin receiving the upload.
    public OutputStream receiveUpload(String filename,
                                      String MIMEType) {
        FileOutputStream fos = null; // Output stream to write to
        file = new File("/tmp/uploads/" + filename);
        try {
            // Open the file for writing.
            fos = new FileOutputStream(file);
        } catch (final java.io.FileNotFoundException e) {
            // Error while opening the file. Not reported here.
            e.printStackTrace();
            return null;
        }

        return fos; // Return the output stream to write to
    }

    // This is called if the upload is finished.
    public void uploadSucceeded(Upload.SucceededEvent event) {
        // Log the upload on screen.
        root.setContent(new Label("File " + event.getFilename()
                + " of type '" + event.getMIMEType()
                + "' uploaded."));

        // Display the uploaded file in the image panel.
        final FileResource imageResource =
                new FileResource(file);
//        imagePanel.removeAllComponents();
        imagePanel.setContent(new Embedded("", imageResource));
    }

    // This is called if the upload fails.
    public void uploadFailed(Upload.FailedEvent event) {
        // Log the failure on screen.
        root.setContent(new Label("Uploading "
                + event.getFilename() + " of type '"
                + event.getMIMEType() + "' failed."));
    }
}