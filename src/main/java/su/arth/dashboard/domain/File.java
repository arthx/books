package su.arth.dashboard.domain;

import javax.persistence.*;

/**
 * Created by arthur on 01.04.16.
 */
@Entity
@Table(name = "file")
public class File {
    private long idFile;
    private String pathfile;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_file")
    public long getIdFile() {
        return idFile;
    }

    public void setIdFile(long idFile) {
        this.idFile = idFile;
    }

    @Basic
    @Column(name = "pathfile")
    public String getPathfile() {
        return pathfile;
    }

    public void setPathfile(String pathfile) {
        this.pathfile = pathfile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        File file = (File) o;

        if (idFile != file.idFile) return false;
        if (pathfile != null ? !pathfile.equals(file.pathfile) : file.pathfile != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idFile ^ (idFile >>> 32));
        result = 31 * result + (pathfile != null ? pathfile.hashCode() : 0);
        return result;
    }
}
