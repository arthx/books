package su.arth.dashboard.domain;

import javax.persistence.*;

/**
 * Created by arthur on 07.04.16.
 */
@Entity
@Table(name = "street")
public class Street {
    private String idStreet;
    private String name;
    private String zip;
    private String okato;
    private Type type;

    @Id
    @Column(name = "id_street")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public String getIdStreet() {
        return idStreet;
    }

    public void setIdStreet(String idStreet) {
        this.idStreet = idStreet;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "zip")
    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Basic
    @Column(name = "okato")
    public String getOkato() {
        return okato;
    }

    public void setOkato(String okato) {
        this.okato = okato;
    }

    @ManyToOne(fetch= FetchType.EAGER,cascade = CascadeType.PERSIST)
    @JoinColumn(name="type_id",referencedColumnName = "id_type")
    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Street street = (Street) o;

        if (idStreet != null ? !idStreet.equals(street.idStreet) : street.idStreet != null) return false;
        if (name != null ? !name.equals(street.name) : street.name != null) return false;
        if (zip != null ? !zip.equals(street.zip) : street.zip != null) return false;
        if (okato != null ? !okato.equals(street.okato) : street.okato != null) return false;
        return type != null ? type.equals(street.type) : street.type == null;

    }

    @Override
    public int hashCode() {
        int result = idStreet != null ? idStreet.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (zip != null ? zip.hashCode() : 0);
        result = 31 * result + (okato != null ? okato.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
