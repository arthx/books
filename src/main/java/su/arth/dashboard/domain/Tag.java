package su.arth.dashboard.domain;

import javax.persistence.*;

/**
 * Created by arthur on 01.04.16.
 */
@Entity
@Table(name = "tag")
public class Tag {
    private long idTag;
    private String tag;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tag")
    public long getIdTag() {
        return idTag;
    }

    public void setIdTag(long idTag) {
        this.idTag = idTag;
    }

    @Basic
    @Column(name = "tag")
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tag tag1 = (Tag) o;

        if (idTag != tag1.idTag) return false;
        if (tag != null ? !tag.equals(tag1.tag) : tag1.tag != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idTag ^ (idTag >>> 32));
        result = 31 * result + (tag != null ? tag.hashCode() : 0);
        return result;
    }
}
