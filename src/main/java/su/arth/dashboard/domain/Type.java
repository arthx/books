package su.arth.dashboard.domain;

import javax.persistence.*;

/**
 * Created by arthur on 07.04.16.
 */
@Entity
@Table(name = "type")
public class Type {
    private int idType;
    private String name;
    private String shortname;

    @Id
    @Column(name = "id_type")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getIdType() {
        return idType;
    }

    public void setIdType(int idType) {
        this.idType = idType;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "shortname")
    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Type type = (Type) o;

        if (idType != type.idType) return false;
        if (name != null ? !name.equals(type.name) : type.name != null) return false;
        if (shortname != null ? !shortname.equals(type.shortname) : type.shortname != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idType;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (shortname != null ? shortname.hashCode() : 0);
        return result;
    }
}
