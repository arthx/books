package su.arth.dashboard.domain;

import javax.persistence.*;

/**
 * Created by arthur on 01.04.16.
 */
@Entity
@Table(name = "settings")
public class Settings {
    private long idSettings;
    private Byte locationShow;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_settings")
    public long getIdSettings() {
        return idSettings;
    }

    public void setIdSettings(long idSettings) {
        this.idSettings = idSettings;
    }

    @Basic
    @Column(name = "location_show")
    public Byte getLocationShow() {
        return locationShow;
    }

    public void setLocationShow(Byte locationShow) {
        this.locationShow = locationShow;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Settings settings = (Settings) o;

        if (idSettings != settings.idSettings) return false;
        if (locationShow != null ? !locationShow.equals(settings.locationShow) : settings.locationShow != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (idSettings ^ (idSettings >>> 32));
        result = 31 * result + (locationShow != null ? locationShow.hashCode() : 0);
        return result;
    }
}
