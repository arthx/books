package su.arth.dashboard.domain;

import javax.persistence.*;

/**
 * Created by arthur on 07.04.16.
 */
@Entity
@Table(name = "district")
public class District {
    private String idDistrict;
    private String name;
    private String zip;
    private String okato;
    private Type type;

    @Id
    @Column(name = "id_district")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public String getIdDistrict() {
        return idDistrict;
    }

    public void setIdDistrict(String idDistrict) {
        this.idDistrict = idDistrict;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "zip")
    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Basic
    @Column(name = "okato")
    public String getOkato() {
        return okato;
    }

    public void setOkato(String okato) {
        this.okato = okato;
    }

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name="type_id",referencedColumnName = "id_type")
    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
