package su.arth.dashboard.event;


import su.arth.dashboard.domain.Book;
import su.arth.dashboard.domain.Location;
import su.arth.dashboard.view.DashboardViewType;
import su.arth.dashboard.view.book.BookContentMenuType;
import su.arth.dashboard.view.book.BookType;

/*
 * Event bus events used in DashboardView are listed here as inner classes.
 */
public abstract class DashboardEvent {

    public static final class UserLoginRequestedEvent {
        private final String userName, password;


        public UserLoginRequestedEvent(final String userName,
                final String password) {
            this.userName = userName;
            this.password = password;
        }


        public String getUserName() {
            return userName;
        }

        public String getPassword() {
            return password;
        }
    }

    public static final class UserSignupRequestedEvent{
        private final String lastname,firstname, email, phone, login,password;
        private final Location location;
        private final byte sex;

        public UserSignupRequestedEvent(byte sex, Location location, String login, String phone, String email, String firstname, String lastname,String password) {
            this.sex = sex;
            this.location = location;
            this.login = login;
            this.phone = phone;
            this.email = email;
            this.firstname = firstname;
            this.lastname = lastname;
            this.password = password;
        }

        public String getLastname() {
            return lastname;
        }

        public String getFirstname() {
            return firstname;
        }

        public String getEmail() {
            return email;
        }

        public String getPhone() {
            return phone;
        }

        public String getLogin() {
            return login;
        }

        public Location getLocation() {
            return location;
        }

        public String getPassword() {
            return password;
        }

        public byte getSex() {
            return sex;
        }
    }

    public static class BrowserResizeEvent {

    }

    public static class UserLoggedOutEvent {

    }

    public static class NotificationsCountUpdatedEvent {
    }

    public static final class ReportsCountUpdatedEvent {
        private final int count;

        public ReportsCountUpdatedEvent(final int count) {
            this.count = count;
        }

        public int getCount() {
            return count;
        }

    }

  /*  public static final class TransactionReportEvent {
        private final Collection<Transaction> transactions;

        public TransactionReportEvent(final Collection<Transaction> transactions) {
            this.transactions = transactions;
        }

        public Collection<Transaction> getTransactions() {
            return transactions;
        }
    }
*/
    public static final class PostViewChangeEvent {
        private final DashboardViewType view;

        public PostViewChangeEvent(final DashboardViewType view) {
            this.view = view;
        }

        public DashboardViewType getView() {
            return view;
        }
    }

    public static class CloseOpenWindowsEvent {
    }

    public static class RemoveBook{
        private final Book book;

        public RemoveBook(Book book) {
            this.book = book;
        }

        public Book getBook() {
            return book;
        }
    }

    public static class AddBook{
        private final Book book;
        private final BookType type;

        public AddBook(Book book,BookType type) {
            this.type = type;
            this.book = book;
        }

        public BookType getType() {
            return type;
        }

        public Book getBook() {
            return book;
        }
    }

    public static class CommandContentMenu{

        private BookContentMenuType view;

        public CommandContentMenu(BookContentMenuType view) {
            this.view = view;
        }

        public BookContentMenuType getView() {
            return view;
        }
    }

    public static class UpdateContentMenu{

        private DashboardViewType view;

        public UpdateContentMenu(DashboardViewType view) {
            this.view = view;
        }

        public DashboardViewType getView() {
            return view;
        }
    }


    public static class ProfileUpdatedEvent {

        private final String lastname,firstname, email, phone, login,password;
         private final byte sex;
        private final long idUser;
        private final Location location;

        public ProfileUpdatedEvent(long idUser,byte sex,Location location, String login, String phone, String email, String firstname, String lastname,String password) {
            this.idUser = idUser;
            this.sex = sex;
            this.location = location;
            this.login = login;
            this.phone = phone;
            this.email = email;
            this.firstname = firstname;
            this.lastname = lastname;
            this.password = password;
        }


        public long getIdUser() {
            return idUser;
        }

        public String getLastname() {
            return lastname;
        }

        public String getFirstname() {
            return firstname;
        }

        public String getEmail() {
            return email;
        }

        public String getPhone() {
            return phone;
        }

        public String getLogin() {
            return login;
        }

        public String getPassword() {
            return password;
        }

        public Location getLocation() {
            return location;
        }

        public byte getSex() {
            return sex;
        }
    }

}
