package su.arth.dashboard.component;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.*;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import su.arth.dashboard.MyUI;
import su.arth.dashboard.domain.Location;
import su.arth.dashboard.domain.User;
import su.arth.dashboard.event.DashboardEvent;
import su.arth.dashboard.event.DashboardEventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by salimhanov on 30.03.2016.
 */

@SuppressWarnings("serial")
public class ProfilePreferencesWindow extends Window {

    public static final String ID = "profilepreferenceswindow";

    private final BeanFieldGroup<User> fieldGroup;
    /*
     * Fields for editing the User object are defined here as class members.
     * They are later bound to a FieldGroup by calling
     * fieldGroup.bindMemberFields(this). The Fields' values don't need to be
     * explicitly set, calling fieldGroup.setItemDataSource(user) synchronizes
     * the fields with the user object.
     */
    @PropertyId("idUser")
    private TextField idUser;
    @PropertyId("firstname")
    private TextField firstnameField;
    @PropertyId("lastname")
    private TextField lastnameField;
    @PropertyId("sex")
    private OptionGroup sexField;
    @PropertyId("email")
    private TextField emailField;
    @PropertyId("location")
    private TextField locationField;
    @PropertyId("phone")
    private TextField phoneField;
    @PropertyId("login")
    private TextField loginField;
    @PropertyId("password")
    private PasswordField passwordField;

     private User user;
    final Embedded image = new Embedded("Uploaded Image");
    Image profilePic;
    //окно пользовательских настроек и изменения данных профиля
    private ProfilePreferencesWindow(final User user,
                                     final boolean preferencesTabOpen) {
        this.user = user;
        addStyleName("profile-window");
        setId(ID);
        Responsive.makeResponsive(this);

        setModal(true);

        setCloseShortcut(ShortcutAction.KeyCode.ESCAPE, null);
        setResizable(false);
        setClosable(true);
        setHeight(90.0f, Unit.PERCENTAGE);
        setWidth(45.0f, Unit.PERCENTAGE);
        VerticalLayout content = new VerticalLayout();
        content.setSizeFull();
        content.setMargin(new MarginInfo(true, false, false, false));
        setContent(content);

        TabSheet detailsWrapper = new TabSheet();
        detailsWrapper.setSizeFull();
        detailsWrapper.addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);
        detailsWrapper.addStyleName(ValoTheme.TABSHEET_ICONS_ON_TOP);
        detailsWrapper.addStyleName(ValoTheme.TABSHEET_CENTERED_TABS);
        content.addComponent(detailsWrapper);
        content.setExpandRatio(detailsWrapper, 1f);

        detailsWrapper.addComponent(buildProfileTab());
        detailsWrapper.addComponent(buildPreferencesTab());

        if (preferencesTabOpen) {
            detailsWrapper.setSelectedTab(1);
        }

        content.addComponent(buildFooter());

        fieldGroup = new BeanFieldGroup<User>(User.class);
        fieldGroup.bindMemberFields(this);
        fieldGroup.setItemDataSource(user);
    }

    private Component buildPreferencesTab() {
        VerticalLayout root = new VerticalLayout();
        root.setCaption("Настройки");
        root.setIcon(FontAwesome.COGS);
        root.setSpacing(true);
        root.setMargin(true);
        root.setSizeFull();

        Label message = new Label("Not implemented in this demo");
        message.setSizeUndefined();
        message.addStyleName(ValoTheme.LABEL_LIGHT);
        root.addComponent(message);
        root.setComponentAlignment(message, Alignment.MIDDLE_CENTER);

        return root;
    }

    private Component buildProfileTab() {
        HorizontalLayout root = new HorizontalLayout();
        root.setCaption("Профиль");
        root.setIcon(FontAwesome.USER);
        root.setWidth(100.0f, Unit.PERCENTAGE);
        root.setSpacing(true);
        root.setMargin(true);
        root.addStyleName("profile-form");

        VerticalLayout pic = new VerticalLayout();
        pic.setSizeUndefined();
        pic.setSpacing(true);
        profilePic = new Image(null, new FileResource(new File(user.getProfile_photo().getPathfile())));
        profilePic.setWidth(100.0f, Unit.PIXELS);
        pic.addComponent(profilePic);


        image.setVisible(false);


//        Button uploadBtn = new Button("Изменить…", event -> upload.startUpload());


       /* uploadBtn.addStyleName(ValoTheme.BUTTON_TINY);
        pic.addComponent(uploadBtn);
*/
        root.addComponent(pic);

        FormLayout details = new FormLayout();
        details.addStyleName(ValoTheme.FORMLAYOUT_LIGHT);
        root.addComponent(details);
        root.setExpandRatio(details, 1);
idUser = new TextField("ид");
        idUser.setVisible(false);
        root.addComponent(idUser);

        firstnameField = new TextField("Имя");
        details.addComponent(firstnameField);
        lastnameField = new TextField("Фамилия");
        details.addComponent(lastnameField);

        loginField = new TextField("Логин");
        details.addComponent(loginField);
        passwordField = new PasswordField("Пароль");
        details.addComponent(passwordField);

        sexField = new OptionGroup("Пол");
        sexField.addItem(0);
        sexField.setItemCaption(0, "Мужской");
        sexField.addItem(1);
        sexField.setItemCaption(1, "Женский");
        sexField.addStyleName("horizontal");
        details.addComponent(sexField);

        Label section = new Label("Контакты");
        section.addStyleName(ValoTheme.LABEL_H4);
        section.addStyleName(ValoTheme.LABEL_COLORED);
        details.addComponent(section);

        emailField = new TextField("Почта");
        emailField.setWidth("100%");
        emailField.setRequired(true);
        emailField.setNullRepresentation("");
        details.addComponent(emailField);

        locationField = new TextField("Адрес");
        locationField.setWidth("100%");
        locationField.setNullRepresentation("");
        locationField.setComponentError(new UserError(
                "This address doesn't exist"));
        details.addComponent(locationField);

        phoneField = new TextField("Телефон");
        phoneField.setWidth("100%");
        phoneField.setNullRepresentation("");
        details.addComponent(phoneField);

        ImageUploader receiver = new ImageUploader();
        //image.setVisible(false);

        Upload upload = new Upload("Изображение профиля", receiver);
        upload.setButtonCaption("Загрузить");
        upload.addSucceededListener(receiver);
        details.addComponent(upload);

        return root;
    }

    private Component buildFooter() {
        HorizontalLayout footer = new HorizontalLayout();
        footer.addStyleName(ValoTheme.WINDOW_BOTTOM_TOOLBAR);
        footer.setWidth(100.0f, Unit.PERCENTAGE);

        Button ok = new Button("Сохранить");
        ok.addStyleName(ValoTheme.BUTTON_PRIMARY);
        ok.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                try {
                    fieldGroup.commit();

                    // Updated user should also be persisted to database. But
                    // not in this demo.

                    Notification success = new Notification(
                            "Профиль обновлен");
                    success.setDelayMsec(3000);
                    success.setStyleName("bar success small");
                    success.setPosition(Position.BOTTOM_CENTER);
                    success.show(Page.getCurrent());


                    DashboardEventBus.post(new DashboardEvent.ProfileUpdatedEvent(Long.valueOf(idUser.getValue()),Byte.parseByte(String.valueOf(sexField.getTabIndex())) ,new Location(),loginField.getValue(), phoneField.getValue(),emailField.getValue(), firstnameField.getValue(),lastnameField.getValue(),passwordField.getValue()));
                    close();
                } catch (FieldGroup.CommitException e) {
                    Notification.show("Ошибка обновления профиля",
                            Notification.Type.ERROR_MESSAGE);
                }

            }
        });
        ok.focus();
        footer.addComponent(ok);
        footer.setComponentAlignment(ok, Alignment.TOP_RIGHT);
        return footer;
    }

    public static void open(final User user, final boolean preferencesTabActive) {
        DashboardEventBus.post(new DashboardEvent.CloseOpenWindowsEvent());
        Window w = new ProfilePreferencesWindow(user, preferencesTabActive);
        UI.getCurrent().addWindow(w);
        w.focus();
    }


    public class ImageUploader implements Upload.Receiver, Upload.SucceededListener {
        public File file;

        public OutputStream receiveUpload(String filename,
                                          String mimeType) {
            // Create upload stream
            FileOutputStream fos = null; // Stream to write to
            try {

               if(!isWindows()){
                    file = new File("/home/arthur/develop/applications/uploads/" + filename);
                } else {
                    file = new File("D:/arthur/android/applications/books/src/main/resources/img" + filename);

                }

                su.arth.dashboard.domain.File fileDomain = new su.arth.dashboard.domain.File();
fileDomain.setPathfile(file.getAbsolutePath());
                MyUI.getDataProvider().addFile(fileDomain,user.getIdUser());
                fos = new FileOutputStream(new File(this.file.getAbsolutePath()));
            } catch (final java.io.FileNotFoundException e) {
                new Notification("Невозможно открыть файл<br/>",
                        e.getMessage(),
                        Notification.Type.ERROR_MESSAGE)
                        .show(Page.getCurrent());
                return null;
            }
            return fos; // Return the output stream to write to
        }

        public void uploadSucceeded(Upload.SucceededEvent event) {
            // Show the uploaded file in the image viewer
            image.setVisible(true);
            image.setSource(new FileResource(file));
             profilePic = new Image(null, new FileResource(file));

        }
    }
    public static boolean isWindows(){

        String os = System.getProperty("os.name").toLowerCase();
        //windows
        return (os.indexOf( "win" ) >= 0);

    }

}
