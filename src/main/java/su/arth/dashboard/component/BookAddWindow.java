package su.arth.dashboard.component;

import com.vaadin.server.Responsive;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import su.arth.dashboard.domain.Book;
import su.arth.dashboard.domain.User;
import su.arth.dashboard.event.DashboardEvent;
import su.arth.dashboard.event.DashboardEventBus;
import su.arth.dashboard.view.book.BookType;

/**
 * Created by arthur on 05.04.16.
 */
public class BookAddWindow extends Window {


    private TextField name;
    private TextField author;
    private TextField publishing;
    private PopupDateField releaseDate;
    private TextField version;
    private TextField score;


    public static final String ID = "bookaddwindow";
    private User user;

    public BookAddWindow(final User user) {
             this.user=user;

        addStyleName("profile-window");
        setId(ID);
        Responsive.makeResponsive(this);

        setModal(false);

       // setCloseShortcut(ShortcutAction.KeyCode.ESCAPE, null);
        setResizable(false);
        setClosable(false);
        setHeight(90.0f, Unit.PERCENTAGE);
        setWidth(45.0f, Unit.PERCENTAGE);
        VerticalLayout content = new VerticalLayout();
        content.setSizeFull();
       content.addComponent(buildWindow());
        setContent(content);



    }


    public Component buildWindow(){
        FormLayout layout = new FormLayout();
        layout.addStyleName(ValoTheme.FORMLAYOUT_LIGHT);


        name = new TextField("Имя");
        layout.addComponent(name);

        author = new TextField("Автор");
        layout.addComponent(author);
        publishing = new TextField("Издательство");
        layout.addComponent(publishing);
        releaseDate = new PopupDateField("Дата");
        layout.addComponent(releaseDate);
        version = new TextField("Версия");
        layout.addComponent(version);

        Button btn = new Button("Создать");

        btn.addClickListener(clickEvent -> DashboardEventBus.post(new DashboardEvent.AddBook(new Book(name.getValue(),author.getValue(),publishing.getValue(),releaseDate.getValue(),version.getValue()), BookType.MYBOOKS)) );
 layout.addComponent(btn);
        return layout;
    }



    public static void open(final User user) {
       try{
        DashboardEventBus.post(new DashboardEvent.CloseOpenWindowsEvent());
        Window w = new BookAddWindow(user);
        UI.getCurrent().addWindow(w);
        w.focus();}
       catch (Exception e ){
           e.printStackTrace();
       }
    }
}
