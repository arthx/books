package su.arth.dashboard.component.suggest;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.filter.UnsupportedFilterException;
import su.arth.dashboard.service.address.KladrObject;

import java.util.List;


public class SuggestingContainer extends BeanItemContainer<KladrObject> {

  private KladrObject defaultCountry;
  private KladrLocationService service;
  private ContainerType containerType;

  public SuggestingContainer(KladrLocationService service){
    super(KladrObject.class);
    this.service = service;
    this.containerType = ContainerType.CITY_CONTAINER;
  }

  public SuggestingContainer(KladrLocationService service,ContainerType containerType) throws IllegalArgumentException {
    super(KladrObject.class);
    this.service = service;
    this.containerType = containerType;
  }

  public SuggestingContainer(KladrLocationService service, KladrObject defaultCountry) throws IllegalArgumentException {
    this(service);
    addBean(defaultCountry);
    this.defaultCountry = defaultCountry;
  }


  @Override
  protected void addFilter(Filter filter) throws UnsupportedFilterException {
    SuggestionFilter suggestionFilter = (SuggestionFilter) filter;
    filterItems(suggestionFilter.getFilterString());
  }

  private void filterItems(String filterString) {
    if ("".equals(filterString)) {
      if (defaultCountry != null) {
         addBean(defaultCountry);
      }
      return;
    }
    List<KladrObject> countries;
    removeAllItems();
    if(containerType==ContainerType.CITY_CONTAINER){
      countries = service.filterCountryTableInDatabase(filterString);
    } else {
       countries = service.filterStreet(filterString);
    }
    addAll(countries);
  }


  public void setSelectedCountryBean(KladrObject country) {
    removeAllItems();
    addBean(country);
  }


  public static class SuggestionFilter implements Filter {

    private String filterString;

    public SuggestionFilter(String filterString) {
      this.filterString = filterString;
    }

    public String getFilterString() {
      return filterString;
    }

    @Override
    public boolean passesFilter(Object itemId, Item item) throws UnsupportedOperationException {
      return false;
    }

    @Override
    public boolean appliesToProperty(Object propertyId) {
      return false;
    }
  }
}
