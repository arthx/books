package su.arth.dashboard.component.suggest;

/**
 * Created by arthur on 09.04.16.
 */
public enum ContainerType {

    STREET_CONTAINER, CITY_CONTAINER

}
