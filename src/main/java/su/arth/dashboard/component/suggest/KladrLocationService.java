package su.arth.dashboard.component.suggest;

import su.arth.dashboard.service.address.KladrObject;

import java.util.List;


public interface KladrLocationService {


  List<KladrObject> filterCountryTableInDatabase(String filterPrefix);

  List<KladrObject> filterStreet(String filterPrefix);

  void setCity(String city);
}
