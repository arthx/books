package su.arth.dashboard.component.suggest;

import su.arth.dashboard.MyUI;
import su.arth.dashboard.domain.City;
import su.arth.dashboard.service.address.KladrLocation;
import su.arth.dashboard.service.address.KladrObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class KladrLocationServiceImpl implements KladrLocationService {

  private String city = "";


  @Override
  public List<KladrObject> filterCountryTableInDatabase(String filterPrefix) {
    if ("".equals(filterPrefix) || filterPrefix == null) {
      return Collections.emptyList();
    }
    List<KladrObject> result = new ArrayList<>();
try {
  List<KladrObject> cityList = KladrLocation.getInstance().getCity(filterPrefix, true);
  if (cityList == null) {
    List<City> cities = MyUI.getDataProvider().getCityList();
    for (City city1 : cities) {
      cityList.add(new KladrObject(city1.getIdCity(), city1.getName(), city1.getZip(), city1.getType().getName(), city1.getType().getShortname(), city1.getOkato()));
    }
  }

  for (KladrObject country : cityList) {
    if (country.getName().toLowerCase().startsWith(filterPrefix)) {
      result.add(country);
    }
  }
} catch (Exception e){

}
    return result;
  }

  @Override
  public List<KladrObject> filterStreet(String filterPrefix) {
    if ("".equals(filterPrefix) || filterPrefix == null) {
      return Collections.emptyList();
    }
    List<KladrObject> result = new ArrayList<>();
    List<KladrObject> streetList = KladrLocation.getInstance().getStreet(city,filterPrefix);
    for (KladrObject street : streetList) {
      if (street.getName().toLowerCase().startsWith(filterPrefix)) {
        result.add(street);
      }
    }

    return result;
  }

  @Override
  public void setCity(String city) {
    this.city = city;
  }
}
