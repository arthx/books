package su.arth.dashboard.component.suggest;

import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.ComboBox;

public class SuggestingComboBox extends ComboBox {

  private boolean type;
  public SuggestingComboBox(String caption) {
    super(caption);
    // the item caption mode has to be PROPERTY for the filtering to work
    setItemCaptionMode(ItemCaptionMode.PROPERTY);
    setItemCaptionPropertyId("title");
  }


  public SuggestingComboBox() {
    this(null);
  }


  @Override
  protected Filter buildFilter(String filterString, FilteringMode filteringMode) {

      return new SuggestingContainer.SuggestionFilter(filterString);

  }
}
