package su.arth.dashboard.service.location;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vaadin.server.VaadinSession;
import com.vaadin.tapio.googlemaps.client.LatLon;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import su.arth.dashboard.MyUI;
import su.arth.dashboard.domain.*;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Created by arthur on 12.04.16.
 */
public class LocationService {


    private static final String KEY = "AIzaSyBeDPh4h4MtSBWiKYAI5mzg1W3WiVlE9lE";


    public String address = "Челябинск";


    private final String USER_AGENT = "Mozilla/5.0";



    public LatLon getMyLocation(){

User user = (User) VaadinSession.getCurrent().getAttribute(User.class.getName());
        MyUI.getDataProvider();
        Location location = user.getLocation();

        StringBuilder addressBuilder = new StringBuilder();
          Region region = location.getRegion();
        if(region!=null) addressBuilder.append(region.getName()+",");
        District district = location.getDistrict();
        if(district!=null) addressBuilder.append(district.getName()+",");
        City city = location.getCity();
        if(city!=null) addressBuilder.append(city.getName()+",");

        Street street = location.getStreet();
        if(street!=null) addressBuilder.append(street.getName());

        House house = location.getHouse();
        if(house!=null) addressBuilder.append(house.getNumber());


        LatLon latLon = null;
        try {
            latLon = getLatLon(addressBuilder.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
         return latLon;
    }




    private LatLon getLatLon(String address) throws Exception {
         String requestStr = "https://maps.googleapis.com/maps/api/geocode/json?address="+URLEncoder.encode(address,"UTF-8")+"&key="+KEY;
        double lat = 55;
        double lon = 56;


        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(requestStr);

        // add request header
        request.addHeader("User-Agent", USER_AGENT);
        HttpResponse response = client.execute(request);
       if(response.getStatusLine().getStatusCode() == 200){

//
           InputStream inputStream = new BufferedInputStream(response.getEntity().getContent());
//           StringBuffer result = new StringBuffer();
//           String line = "";

          String result = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
//           while ((line = rd.readLine()) != null) {
//               result.append(line);
//           }



           JsonParser parser = new JsonParser();
           JsonObject mainObject = parser.parse(result.toString()).getAsJsonObject();
           JsonArray addressItem = mainObject.getAsJsonArray("results");

           JsonObject jsonObject = (JsonObject) mainObject.getAsJsonArray("results").getAsJsonArray().get(0).getAsJsonObject().getAsJsonObject("geometry").get("location");
           lat = jsonObject.get("lat").getAsDouble();
           lon = jsonObject.get("lng").getAsDouble();
       }

        return new LatLon(lat,lon);

    }

}
