package su.arth.dashboard.service.location;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Key;

import java.util.List;
import java.util.Map;

/**
 * Created by arthur on 12.04.16.
 */
public class HttpGoogleTest {
    static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    static final JsonFactory JSON_FACTORY = new JacksonFactory();



    /** Represents a video. */
    public static class ResultObject  {

        @Key
        public List results;

        @Key
        public String status;


    }

    public static class Address {
        @Key
        public List<String> address_components;

        @Key
        public String formatted_address;
        @Key
        public Geometry geometry;


        @Key
        public String place_id;

    }
    public static class Geometry {
        @Key
        public Location location;

        @Key
        public String location_type;

        @Key
        public String viewport;



    }

    public static class Location {

        @Key
        public String lat;
        @Key
        public String lng;


    }


  public static class LocaionHttp extends GenericUrl{
      public LocaionHttp(String encodedUrl) {
          super(encodedUrl);
      }

      @Key
      public String fields;

  }


    private static void run() throws Exception {
        HttpRequestFactory requestFactory =
                HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
                    @Override
                    public void initialize(HttpRequest request) {
                        request.setParser(new JsonObjectParser(JSON_FACTORY));
                    }
                });
        String request_url = "https://maps.googleapis.com/maps/api/geocode/json?address=Челябинск,Братьев Кашириных,99б&key=AIzaSyBeDPh4h4MtSBWiKYAI5mzg1W3WiVlE9lE";
        LocaionHttp url = new LocaionHttp(request_url);
        url.fields = "status,results";
        HttpRequest request = requestFactory.buildGetRequest(url);
        ResultObject resultObject = request.execute().parseAs(ResultObject.class);

        List results = request.execute().parseAs(ResultObject.class).results;


        for(Object o:results){
                Address address = (Address) o;
                  String lat = address.geometry.location.lat;

           }
      /*  if (.isEmpty()) {
            System.out.println("No videos found.");
        } else {*/
        /*    if (location.hasMore) {
                System.out.print("First ");
            }
            System.out.println(location.list.size() + " videos found:");
            for (Video video : location.list) {
                System.out.println();
                System.out.println("-----------------------------------------------");
                System.out.println("ID: " + video.id);
                System.out.println("Title: " + video.title);
                System.out.println("Tags: " + video.tags);
                System.out.println("URL: " + video.url);
            }
        }*/
    //}
    }

    public static void main(String[] args) {
        try {
            try {
                run();
                return;
            } catch (HttpResponseException e) {
                System.err.println(e.getMessage());
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
        System.exit(1);
    }
}
