package su.arth.dashboard.repository;

import java.util.List;

/**
 * Created by salimhanov on 30.03.2016.
 */
public interface Repository<T> {


    void create(T entity);

    void update(T entity);

    void delete(long id);

    List<T> findAll();

}
