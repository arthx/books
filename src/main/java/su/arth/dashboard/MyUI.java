package su.arth.dashboard;

import com.google.common.eventbus.Subscribe;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.*;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;
import su.arth.dashboard.domain.Role;
import su.arth.dashboard.domain.RoleType;
import su.arth.dashboard.domain.User;
import su.arth.dashboard.event.DashboardEvent;
import su.arth.dashboard.event.DashboardEventBus;
import su.arth.dashboard.provider.DataProvider;
import su.arth.dashboard.provider.EntityDataProvider;
import su.arth.dashboard.view.LoginView;
import su.arth.dashboard.view.MainView;

import javax.servlet.annotation.WebServlet;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
@Widgetset("su.arth.MyAppWidgetset")
public class MyUI extends UI {

    private final DashboardEventBus dashboardEventbus = new DashboardEventBus();
    private final DataProvider dataProvider = new EntityDataProvider();
    public static final String PERSISTENCE_UNIT = "books";





    @Override
    protected void init(VaadinRequest vaadinRequest) {

        DashboardEventBus.register(this);
        Responsive.makeResponsive(this);
        addStyleName(ValoTheme.UI_WITH_MENU);

        updateContent();

        Page.getCurrent().addBrowserWindowResizeListener(event -> DashboardEventBus.post(new DashboardEvent.BrowserResizeEvent()));




    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {


    }



    public static DashboardEventBus getDashboardEventbus() {
        return ((MyUI) getCurrent()).dashboardEventbus;
    }

    private void updateContent() {
        User user = (User) VaadinSession.getCurrent().getAttribute(
                User.class.getName());
        if (user != null) {
            for(Role role:user.getRoles()){
                String rolename =role.getRolename();
                if(rolename.equals(RoleType.ROLE_USER.getRoleType()) || rolename.equals(RoleType.ROLE_ADMIN.getRoleType())){
                    setContent(new MainView());
                    removeStyleName("loginview");
                    getNavigator().navigateTo(getNavigator().getState());
                    break;
                }
            }

        } else {
            setContent(new LoginView());
            addStyleName("loginview");
        }
    }


    @Subscribe
    public void userLoginRequested(final DashboardEvent.UserLoginRequestedEvent event) {
        User user = getDataProvider().authenticate(event.getUserName(),
                event.getPassword());
        VaadinSession.getCurrent().setAttribute(User.class.getName(), user);
        updateContent();
    }

    @Subscribe
    public void userSignupRequested(final DashboardEvent.UserSignupRequestedEvent event) {

      User user = getDataProvider().registredUser(new User(event.getLogin(), event.getPassword(), event.getLastname(), event.getFirstname(), event.getEmail(), event.getPhone(), event.getLocation(), event.getSex()));
if(user!=null){
    userLoginRequested(new DashboardEvent.UserLoginRequestedEvent(event.getLogin(),event.getPassword()));
}


    }


    @Subscribe
    public void userUpdatedProfiles(final DashboardEvent.ProfileUpdatedEvent event){
        User user = getDataProvider().updatedUser(new User(event.getLogin(), event.getPassword(), event.getLastname(), event.getFirstname(), event.getEmail(), event.getPhone(), event.getLocation(), event.getSex()));

    }


    @Subscribe
    public void userLoggedOut(final DashboardEvent.UserLoggedOutEvent event) {
        // When the user logs out, current VaadinSession gets closed and the
        // page gets reloaded on the login screen. Do notice the this doesn't
        // invalidate the current HttpSession.
        VaadinSession.getCurrent().close();
        Page.getCurrent().reload();
    }

    @Subscribe
    public void closeOpenWindows(final DashboardEvent.CloseOpenWindowsEvent event) {
        for (Window window : getWindows()) {
            window.close();
        }
    }

    @Subscribe
    public void addBook(final DashboardEvent.AddBook event){
   if(event.getType().getCode()==1) {
       getDataProvider().addMyBook(event.getBook());
   } else {
       getDataProvider().addWishBook(event.getBook());
   }
    }

    @Subscribe
    public void removeBook(final DashboardEvent.RemoveBook event){
        getDataProvider().removeMyBook(event.getBook());
    }


    public static DataProvider getDataProvider() {
        return ((MyUI) getCurrent()).dataProvider;
    }
}
