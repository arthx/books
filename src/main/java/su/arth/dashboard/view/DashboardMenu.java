package su.arth.dashboard.view;

import com.google.common.eventbus.Subscribe;
import com.vaadin.server.FileResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.AlignmentInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.themes.ValoTheme;
import su.arth.dashboard.component.ProfilePreferencesWindow;
import su.arth.dashboard.domain.User;
import su.arth.dashboard.event.DashboardEvent;
import su.arth.dashboard.event.DashboardEventBus;
import su.arth.dashboard.view.book.BookContentMenuType;

import java.io.File;

/**
 * A responsive menu component providing user information and the controls for
 * primary navigation between the views.
 */
@SuppressWarnings({ "serial", "unchecked" })
public final class DashboardMenu extends CustomComponent {

    public static final String ID = "dashboard-menu";
    public static final String REPORTS_BADGE_ID = "dashboard-menu-reports-badge";
    public static final String NOTIFICATIONS_BADGE_ID = "dashboard-menu-notifications-badge";
    private static final String STYLE_VISIBLE = "valo-menu-visible";
    private Label notificationsBadge;
    private Label reportsBadge;
    private MenuItem settingsItem;
   private Component contentMenu;

    public DashboardMenu() {
        setPrimaryStyleName("valo-menu");
        setId(ID);
        setSizeUndefined();

        // There's only one DashboardMenu per UI so this doesn't need to be
        // unregistered from the UI-scoped DashboardEventBus.
        DashboardEventBus.register(this);

        setCompositionRoot(buildContent());
    }

    private Component buildContent() {
        final CssLayout menuContent = new CssLayout();
        menuContent.addStyleName("sidebar");
        menuContent.addStyleName(ValoTheme.MENU_PART);
        menuContent.addStyleName("no-vertical-drag-hints");
        menuContent.addStyleName("no-horizontal-drag-hints");
        menuContent.setWidth(null);
        menuContent.setHeight("100%");

        menuContent.addComponent(buildTitle());
        menuContent.addComponent(buildUserMenu());
        menuContent.addComponent(buildToggleButton());
        menuContent.addComponent(buildMenuItems());
        VerticalLayout verticalLayout = new VerticalLayout();
        contentMenu = buildContentMenu(DashboardViewType.DASHBOARD);
        verticalLayout.addComponent(contentMenu);
        verticalLayout.setSizeFull();



        verticalLayout.setComponentAlignment(contentMenu, new Alignment(AlignmentInfo.Bits.ALIGNMENT_BOTTOM));
         //menuContent.addComponent(contentMenu);
        menuContent.addComponent(verticalLayout);

        return menuContent;
    }

    private Component buildTitle() {
        Label logo = new Label("Портал для поиска книг",
                ContentMode.HTML);
        logo.setSizeUndefined();
        HorizontalLayout logoWrapper = new HorizontalLayout(logo);
        logoWrapper.setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
        logoWrapper.addStyleName("valo-menu-title");
        return logoWrapper;
    }

    private User getCurrentUser() {
        return (User) VaadinSession.getCurrent().getAttribute(
                User.class.getName());
    }

    private Component buildUserMenu() {
        final MenuBar settings = new MenuBar();
        settings.addStyleName("user-menu");
        final User user = getCurrentUser();
        settingsItem = settings.addItem("",new FileResource(new File(user.getProfile_photo().getPathfile())), null);
        updateUserName(null);
        settingsItem.addItem("Редактировать профиль", selectedItem-> ProfilePreferencesWindow.open(user, false));
        settingsItem.addItem("Настройки", new Command() {
            @Override
            public void menuSelected(final MenuItem selectedItem) {
                ProfilePreferencesWindow.open(user, true);
            }
        });
        settingsItem.addSeparator();
        settingsItem.addItem("Выйти", new Command() {
            @Override
            public void menuSelected(final MenuItem selectedItem) {
                DashboardEventBus.post(new DashboardEvent.UserLoggedOutEvent());
            }
        });
        return settings;
    }

    private Component buildToggleButton() {
        Button valoMenuToggleButton = new Button("Menu", new ClickListener() {
            @Override
            public void buttonClick(final ClickEvent event) {
                if (getCompositionRoot().getStyleName().contains(STYLE_VISIBLE)) {
                    getCompositionRoot().removeStyleName(STYLE_VISIBLE);
                } else {
                    getCompositionRoot().addStyleName(STYLE_VISIBLE);
                }
            }
        });
        valoMenuToggleButton.setIcon(FontAwesome.LIST);
        valoMenuToggleButton.addStyleName("valo-menu-toggle");
        valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_BORDERLESS);
        valoMenuToggleButton.addStyleName(ValoTheme.BUTTON_SMALL);
        return valoMenuToggleButton;
    }

    private Component buildMenuItems() {
        CssLayout menuItemsLayout = new CssLayout();
        menuItemsLayout.addStyleName("valo-menuitems");

        for (final DashboardViewType view : DashboardViewType.values()) {
            Component menuItemComponent = new ValoMenuItemButton(view);

            menuItemsLayout.addComponent(menuItemComponent);
        }
        return menuItemsLayout;

    }


    public Component buildContentMenu(DashboardViewType view) {
        CssLayout menuItemsLayout = new CssLayout();
        menuItemsLayout.addStyleName("valo-menuitems");

        for (final BookContentMenuType viewBook : BookContentMenuType.values()) {
           Component menuItemComponent = new ContentMenuItemButton(viewBook);
                       menuItemsLayout.addComponent(menuItemComponent);

        }

             return menuItemsLayout;
    }


    private Component buildBadgeWrapper(final Component menuItemButton,
                                        final Component badgeLabel) {
        CssLayout dashboardWrapper = new CssLayout(menuItemButton);
        dashboardWrapper.addStyleName("badgewrapper");
        dashboardWrapper.addStyleName(ValoTheme.MENU_ITEM);
        badgeLabel.addStyleName(ValoTheme.MENU_BADGE);
        badgeLabel.setWidthUndefined();
        badgeLabel.setVisible(false);
        dashboardWrapper.addComponent(badgeLabel);
        return dashboardWrapper;
    }

    @Override
    public void attach() {
        super.attach();
        //updateNotificationsCount(null);
    }

    /*@Subscribe
    public void postViewChange(final PostViewChangeEvent event) {
        // After a successful view change the menu can be hidden in mobile view.
        getCompositionRoot().removeStyleName(STYLE_VISIBLE);
    }*/

   /* @Subscribe
    public void updateNotificationsCount(
            final DashboardEvent.NotificationsCountUpdatedEvent event) {
        int unreadNotificationsCount = MyUI.getDataProvider()
                .getUnreadNotificationsCount();
        notificationsBadge.setValue(String.valueOf(unreadNotificationsCount));
        notificationsBadge.setVisible(unreadNotificationsCount > 0);
    }
*/


    @Subscribe
    public void updateUserName(final DashboardEvent.ProfileUpdatedEvent event) {
        User user = getCurrentUser();
        settingsItem.setText(user.getFirstname() + " " + user.getLastname());
        settingsItem.setIcon(new FileResource(new File(user.getProfile_photo().getPathfile())));
    }



    public final class ContentMenuItemButton extends Button {

       // private static final String STYLE_SELECTED = "selected";

        private final BookContentMenuType view;

        public ContentMenuItemButton(final BookContentMenuType view) {
            this.view = view;
            setPrimaryStyleName("valo-menu-item");
            setIcon(view.getIcon());
            setCaption(view.getViewName().substring(0, 1).toUpperCase()
                    + view.getViewName().substring(1));
            DashboardEventBus.register(this);
            addClickListener(event -> {
                //buildContentMenu(view);
                DashboardEventBus.post(new DashboardEvent.CommandContentMenu(view));
                //UI.getCurrent().getNavigator().navigateTo(view.getViewName());
            });
        }



        @Subscribe
        public void updateContentMenu(final DashboardEvent.UpdateContentMenu event){
            if(event.getView() == DashboardViewType.DASHBOARD){
                contentMenu.setVisible(true);
            } else {
                contentMenu.setVisible(false);
            }

        }


    }




    public final class ValoMenuItemButton extends Button {

        private static final String STYLE_SELECTED = "selected";

        private final DashboardViewType view;

        public ValoMenuItemButton(final DashboardViewType view) {
            this.view = view;
            setPrimaryStyleName("valo-menu-item");
            setIcon(view.getIcon());
            setCaption(view.getViewName().substring(0, 1).toUpperCase()
                    + view.getViewName().substring(1));
            DashboardEventBus.register(this);


            addClickListener(event -> {
                DashboardEventBus.post(new DashboardEvent.UpdateContentMenu(view));
                UI.getCurrent().getNavigator().navigateTo(view.getViewName());
            });

        }

        @Subscribe
        public void postViewChange(final DashboardEvent.PostViewChangeEvent event) {
            removeStyleName(STYLE_SELECTED);
            if (event.getView() == view) {
                addStyleName(STYLE_SELECTED);
            }
        }


    }




}
