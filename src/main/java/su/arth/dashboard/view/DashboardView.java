package su.arth.dashboard.view;

import com.google.common.eventbus.Subscribe;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.LayoutEvents;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FileResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import su.arth.dashboard.MyUI;
import su.arth.dashboard.domain.Book;
import su.arth.dashboard.event.DashboardEvent;
import su.arth.dashboard.event.DashboardEventBus;
import su.arth.dashboard.view.book.BookContentMenuType;
import su.arth.dashboard.view.book.BookType;

import java.io.File;
import java.util.List;

/**
 * Created by salimhanov on 28.03.2016.
 */
public class DashboardView extends TabSheet implements View,TabSheet.CloseHandler {



   public DashboardView(){
        setSizeFull();
        addStyleName("schedule");
       addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);
       setCloseHandler(this);
       DashboardEventBus.register(this);
       addComponent(buildCatalogViewMyBooks(BookType.MYBOOKS));
       addComponent(buildCatalogViewMyBooks(BookType.WISHBOOKS));
       addComponent(buildSearchPanel());

       /*Component tray = buildTray();
        addComponent(tray);*/
    }

    private Component buildPanelMyBooks(){
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        Button addBook = new Button("Удалить книгу");



        addBook.addClickListener(clickEvent ->
                new DashboardEvent.RemoveBook(new Book())
        );
        addBook.setStyleName(ValoTheme.BUTTON_BORDERLESS_COLORED);

        horizontalLayout.addComponent(addBook);

        return horizontalLayout;
    }

    private Component buildSearchPanel(){
        CssLayout catalog = new CssLayout();
        catalog.setCaption("Поиск книг");
        catalog.addStyleName("catalog");
catalog.setSizeFull();
        VerticalLayout verticalLayout = new VerticalLayout();
        HorizontalLayout horizontalLayout = new HorizontalLayout();


        horizontalLayout.setMargin(true);

             TextField searchField = new TextField();
//        searchField.setCaption("Поиск");
        searchField.setIcon(FontAwesome.SEARCH);
        searchField.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);

        Button searchButton = new Button("Найти");

        searchButton.addClickListener(clickEvent -> {

        });

        horizontalLayout.addComponent(searchField);
        horizontalLayout.addComponent(searchButton);

        List<Book> bookList = MyUI.getDataProvider().getBooksAll();

        BeanItemContainer<Book> container = new BeanItemContainer<Book>(Book.class,bookList);
//verticalLayout.setSizeFull();
        Grid booksGrid = new Grid(container);
//        booksGrid.



 booksGrid.setColumns("name","author","publishing","releaseDate");

//        Grid.HeaderRow row = booksGrid.prependHeaderRow();
      //  row.join("name", "author").setHtml("<b>Full name</b>");
        booksGrid.getDefaultHeaderRow().getCell("name").setHtml("<b>Название</b>");
        booksGrid.getDefaultHeaderRow().getCell("author").setHtml("<b>Автор</b>");
        booksGrid.getDefaultHeaderRow().getCell("publishing").setHtml("<b>Издательство</b>");
        booksGrid.getDefaultHeaderRow().getCell("releaseDate").setHtml("<b>Дата издания</b>");


        booksGrid.addItemClickListener(itemClickEvent -> {
                    Window window = new Window();
                    window.setPosition(itemClickEvent.getClientX(), itemClickEvent.getClientY());
                    window.setHeight(15, Unit.PERCENTAGE);
                    window.setWidth(20, Unit.PERCENTAGE);
                    VerticalLayout layout = new VerticalLayout();
                    window.setModal(true);
                    window.setContent(layout);
                    UI.getCurrent().addWindow(window);
                    window.focus();
                    window.setClosable(false);
                    window.setResizable(false);


                    Button newWishBookBtn = new Button("Добавить в желаемые");
                    Button tradeBtn = new Button("Создать обмен");
                    Button cancelBtn = new Button("Отмена");


                    newWishBookBtn.addStyleName(ValoTheme.BUTTON_BORDERLESS);
                    tradeBtn.addStyleName(ValoTheme.BUTTON_BORDERLESS);
                    cancelBtn.addStyleName(ValoTheme.BUTTON_BORDERLESS);
                    cancelBtn.setSizeFull();
                    tradeBtn.setSizeFull();
                    newWishBookBtn.setSizeFull();

                    tradeBtn.addClickListener(clickEvent -> {
                        window.close();
                    });
                    newWishBookBtn.addClickListener(clickEvent -> {
                        DashboardEventBus.post(new DashboardEvent.AddBook((Book) itemClickEvent.getItemId(), BookType.WISHBOOKS));
                        window.close();
                    });
                    cancelBtn.addClickListener(clickEvent -> window.close());

                    layout.addComponents(newWishBookBtn, tradeBtn, cancelBtn);

               /*     menuBar.addStyleName("user-menu");

               MenuBar.MenuItem menuItem = menuBar.addItem("Добавить в желаемые", new MenuBar.Command() {
                        @Override
                        public void menuSelected(MenuBar.MenuItem menuItem) {
                            DashboardEventBus.post(new DashboardEvent.AddBook((Book) itemClickEvent.getItemId(), BookType.WISHBOOKS));
                        }
                    });

            MenuBar.MenuItem tradeItem =   menuBar.addItem("Назначить обмен", new MenuBar.Command() {
                 @Override
                 public void menuSelected(MenuBar.MenuItem menuItem) {

                 }
             });
            menuItem.addSeparator();
            menuBar.addItem("Отмена", new MenuBar.Command() {
                @Override
                public void menuSelected(MenuBar.MenuItem menuItem) {
                    window.close();
                }
            });

                    });
*/
//        Page.getCurrent().getStyles().add(".footer-right { text-align: right }");
//        footer.setStyleName("footer-right");
                });
        verticalLayout.setSizeFull();
        booksGrid.setSizeFull();
        verticalLayout.addComponent(booksGrid);
     verticalLayout.addComponent(horizontalLayout);

        catalog.addComponent(verticalLayout);

        return catalog;
    }



    private Component buildListBooks(){
        return null;
    }




    private Component buildCatalogViewMyBooks(BookType type) {
        CssLayout catalog = new CssLayout();
        catalog.setCaption(type.getName());
        catalog.addStyleName("catalog");
       catalog.addComponent(buildPanelMyBooks());

List<Book> bookList ;

        if(type.getCode()==1){
            bookList = MyUI.getDataProvider().getBooksByUser();
        } else {
            bookList = MyUI.getDataProvider().getWishBookByUser();
        }


        for (final su.arth.dashboard.domain.Book book : bookList) {

            VerticalLayout frame = new VerticalLayout();

            frame.setMargin(new MarginInfo(true, false, false, true));
//            frame.setPrimaryStyleName("valo-menu-item");
            frame.setPrimaryStyleName("user-menu");
            frame.setWidthUndefined();


            Image poster = new Image(null, new FileResource(new File(book.getImage().getPathfile())));
            poster.setWidth(100.0f, Unit.PIXELS);
            poster.setHeight(145.0f, Unit.PIXELS);
            frame.addComponent(poster);

            Label titleLabel = new Label(book.getName());
            titleLabel.setWidth(120.0f, Unit.PIXELS);
            frame.addComponent(titleLabel);



            frame.addLayoutClickListener((LayoutEvents.LayoutClickListener) event -> {
                if (event.getButton() == MouseEventDetails.MouseButton.LEFT) {
                 
                  Notification.show("test click");
                   // MovieDetailsWindow.open(book, null, null);
                }



            });
            catalog.addComponent(frame);
        }
        //catalog.addComponent(verticalLayout);
        return catalog;
    }


    private Component buildTabNewBook(){
        CssLayout catalog = new CssLayout();
        catalog.setCaption("Новая книга");
        catalog.addStyleName("catalog");

        FormLayout layout = new FormLayout();
        layout.addStyleName(ValoTheme.FORMLAYOUT_LIGHT);


       TextField name = new TextField("Имя");
        layout.addComponent(name);

        TextField author = new TextField("Автор");
        layout.addComponent(author);
        TextField publishing = new TextField("Издательство");
        layout.addComponent(publishing);
        DateField releaseDate = new DateField("Дата");
        layout.addComponent(releaseDate);
        TextField version = new TextField("Версия");
        layout.addComponent(version);

        Button btn = new Button("Создать");

        btn.addClickListener(clickEvent -> {
            DashboardEventBus.post(new DashboardEvent.AddBook(new su.arth.dashboard.domain.Book(name.getValue(),author.getValue(),publishing.getValue(),releaseDate.getValue(),version.getValue()),BookType.MYBOOKS));
                             onTabClose(this,catalog);
            Page.getCurrent().reload();
        } );

        layout.addComponent(btn);
        catalog.addComponent(layout);
      return catalog;
    }

    private Component buildTray() {
        final HorizontalLayout tray = new HorizontalLayout();
        tray.setWidth(100.0f, Unit.PERCENTAGE);
        tray.addStyleName("tray");
        tray.setSpacing(true);
        tray.setMargin(true);

        Label warning = new Label("You have unsaved changes made to the schedule");
        warning.addStyleName("warning");
        warning.addStyleName("icon-attention");
        tray.addComponent(warning);
        tray.setComponentAlignment(warning, Alignment.MIDDLE_LEFT);
        tray.setExpandRatio(warning, 1);

        Button.ClickListener close = new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
                setTrayVisible(false);
            }
        };

        Button confirm = new Button("Confirm");
        confirm.addStyleName(ValoTheme.BUTTON_PRIMARY);
        confirm.addClickListener(close);
        tray.addComponent(confirm);
        tray.setComponentAlignment(confirm, Alignment.MIDDLE_LEFT);

        Button discard = new Button("Discard");
        discard.addClickListener(close);
        discard.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
//                calendar.markAsDirty();
            }
        });
        tray.addComponent(discard);
        tray.setComponentAlignment(discard, Alignment.MIDDLE_LEFT);
        return tray;
    }

    private void setTrayVisible(final boolean visible) {
        final String styleReveal = "v-animate-reveal";
        if (visible) {
//            tray.addStyleName(styleReveal);
        } else {
//            tray.removeStyleName(styleReveal);
        }
    }


    @Subscribe
    public void commandContentMenu(final DashboardEvent.CommandContentMenu event){
        try {
            if (event.getView() == BookContentMenuType.NEW_BOOK) {
                     Tab tab = this.getTab(3);
                     if(tab==null){
                        Tab newbook = addTab(buildTabNewBook(),3);
                         newbook.setClosable(true);
                   this.setSelectedTab(newbook);
                     }

            }
            if (event.getView() == BookContentMenuType.EDIT_BOOK) {

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Subscribe
    public void browserWindowResized(final DashboardEvent.BrowserResizeEvent event) {
        if (Page.getCurrent().getBrowserWindowWidth() < 800) {
//            calendar.setEndDate(calendar.getStartDate());
        }
    }

    @Override
    public void enter(final ViewChangeListener.ViewChangeEvent event) {
    }

    @Override
    public void onTabClose(TabSheet tabSheet, Component component) {
        if (component instanceof TabSheet.CloseHandler) {
            ((TabSheet.CloseHandler) component).onTabClose(tabSheet, component);
        }
        tabSheet.removeComponent(component);
    }
}
