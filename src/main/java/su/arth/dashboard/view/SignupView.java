package su.arth.dashboard.view;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import su.arth.dashboard.MyUI;
import su.arth.dashboard.component.suggest.*;
import su.arth.dashboard.domain.*;
import su.arth.dashboard.event.DashboardEvent;
import su.arth.dashboard.event.DashboardEventBus;
import su.arth.dashboard.service.address.KladrLocation;
import su.arth.dashboard.service.address.KladrObject;

import java.util.List;

/**
 * Created by salimhanov on 31.03.2016.
 */
public class SignupView extends VerticalLayout {

    private Location location;
    private City city;
    private Street street;
    private Region region;
    private District district;
    private House house;
    private KladrObject regionObject;
    private KladrObject districtObject;
    private KladrObject locationObject;
    private KladrObject streetObject;
    private KladrObject cityObject;
    private Button save;

    public SignupView() {
        setSizeFull();
        Component signupForm = buildSignupForm();
        addComponent(signupForm);

        setComponentAlignment(signupForm, Alignment.MIDDLE_CENTER);

        Notification notification = new Notification(
                "Добро пожаловать");
        notification
                .setDescription("<span>Введите свои данные и нажмите сохранить</span>");
        notification.setHtmlContentAllowed(true);
        notification.setStyleName("tray dark small closable login-help");
        notification.setPosition(Position.BOTTOM_CENTER);
        notification.setDelayMsec(5000);
        notification.show(Page.getCurrent());

    }



    private Component buildSignupForm() {

        final VerticalLayout signupPanel = new VerticalLayout();
        final HorizontalLayout btnPanel = new HorizontalLayout();

       signupPanel.setSpacing(true);
        signupPanel.setSizeUndefined();
        Responsive.makeResponsive(signupPanel);
        signupPanel.addStyleName("login-panel");

        Button back = new Button("Назад");
        back.addStyleName(ValoTheme.BUTTON_BORDERLESS);
        back.addClickListener(clickEvent ->
            Page.getCurrent().reload()
        );
        back.setClickShortcut(ShortcutAction.KeyCode.ESCAPE);

        save = new Button("Сохранить");

        save.addStyleName(ValoTheme.BUTTON_BORDERLESS);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        save.focus();
        btnPanel.setSpacing(true);

        btnPanel.addComponent(back);
        btnPanel.addComponent(save);
        btnPanel.setComponentAlignment(save, Alignment.BOTTOM_RIGHT);
        signupPanel.addComponent(buildFields());
        signupPanel.addComponent(btnPanel);



        return signupPanel;
    }


    private class LoginValidator implements Validator{
        @Override
        public void validate(Object value) throws InvalidValueException {
            if (value instanceof String && MyUI.getDataProvider().isLogin((String) value)) {

                save.setEnabled(false);
                throw new InvalidValueException("Логин занят");
            } else {
                save.setEnabled(true);
            }
        }
    }


    private Component buildFields() {
        HorizontalLayout layoutInfo = new HorizontalLayout();

        VerticalLayout signFields = new VerticalLayout();
        signFields.setMargin(new MarginInfo(true, true, false, false));

        signFields.setSpacing(true);
        signFields.addStyleName("fields");

        VerticalLayout infoFields = new VerticalLayout();
        infoFields.setSpacing(true);
        infoFields.setMargin(new MarginInfo(true, true, false, false));


        VerticalLayout contactFields = new VerticalLayout();
        contactFields.setSpacing(true);
        contactFields.setMargin(new MarginInfo(true, true, false, false));

        final TextField login = new TextField("Логин");
        login.setIcon(FontAwesome.USER);
        login.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);
        login.addValidator(new LoginValidator());

        login.setMaxLength(45);
        login.setRequired(true);
            login.setTextChangeEventMode(AbstractTextField.TextChangeEventMode.LAZY);


        final PasswordField password = new PasswordField("Пароль");
        password.setIcon(FontAwesome.LOCK);
        password.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);

        final PasswordField passwordField = new PasswordField("Пароль");
        passwordField.setIcon(FontAwesome.LOCK);
        passwordField.addStyleName(ValoTheme.TEXTFIELD_INLINE_ICON);

        final TextField lastname = new TextField("Фамилия");
        lastname.addStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);

        final TextField firstname = new TextField("Имя");
        firstname.addStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);

        final OptionGroup sex = new OptionGroup("Пол");
        sex.addItem(0);
        sex.setItemCaption(0,"М");
        sex.addItem(1);
        sex.setItemCaption(1,"Ж");
        sex.addStyleName("horizontal");
        sex.setNullSelectionAllowed(false);
        sex.setHtmlContentAllowed(true);
        sex.setImmediate(true);

        final TextField numberHouse = new TextField("Дом");
         numberHouse.addStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);
        numberHouse.setVisible(false);
        final KladrLocationService kladrLocationService = new KladrLocationServiceImpl();


        final TextField streetStr = new TextField("Введите улицу");
        streetStr.setVisible(false);


        final SuggestingContainer container = new SuggestingContainer(kladrLocationService);
        final SuggestingComboBox cityCB = new SuggestingComboBox("Введите город");
        cityCB.setFilteringMode (FilteringMode.CONTAINS);

        cityCB.setImmediate(true);
        cityCB.setContainerDataSource(container);

 final SuggestingComboBox streetComboBox = new SuggestingComboBox("Введите улицу");
 final SuggestingContainer street_container = new SuggestingContainer(kladrLocationService,ContainerType.STREET_CONTAINER);
        streetComboBox.setFilteringMode(FilteringMode.CONTAINS);
        streetComboBox.setImmediate(true);
        streetComboBox.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
               if(!"".equals(event.getProperty().getValue())){
                   Notification.show("Вы выбрали: " + event.getProperty().getValue(), Notification.Type.HUMANIZED_MESSAGE);

               }
                street_container.setSelectedCountryBean((KladrObject) event.getProperty().getValue());
                streetObject = (KladrObject) event.getProperty().getValue();


            }
        });

        cityCB.setRequired(true);
        cityCB.setRequiredError("Введите значение");
        streetComboBox.setRequired(true);
        streetComboBox.setRequiredError("Введите значение");

        password.setRequired(true);
        passwordField.setRequired(true);
        lastname.setRequired(true);
        firstname.setRequired(true);


        cityCB.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                if(event.getProperty().getValue()!=null && !"".equals(event.getProperty().getValue())) {
                    Notification.show("Вы выбрали: " + event.getProperty().getValue(), Notification.Type.HUMANIZED_MESSAGE);
                    streetComboBox.setVisible(true);
                    numberHouse.setVisible(true);
                } else {
                    streetComboBox.setVisible(false);
                    numberHouse.setVisible(false);
                }
                streetComboBox.removeAllItems();
                container.setSelectedCountryBean((KladrObject) event.getProperty().getValue());

                if(!KladrLocation.getInstance().isStreetsInCity(((KladrObject) event.getProperty().getValue()).getName())){
                    streetComboBox.setVisible(false);
                    streetComboBox.setValidationVisible(false);
                    streetStr.setVisible(true);

                } else {
                    streetComboBox.setVisible(true);
                    streetStr.setVisible(false);
                }
                try {

                    kladrLocationService.setCity(((KladrObject) event.getProperty().getValue()).getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                cityObject = (KladrObject) event.getProperty().getValue();

            }
        });


        streetComboBox.setContainerDataSource(street_container);
streetComboBox.setVisible(false);

        final TextField email = new TextField("Почта");
        email.setIcon(FontAwesome.MAIL_FORWARD);
        lastname.addStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);

        final TextField phone = new TextField("Телефон");
        phone.setIcon(FontAwesome.MOBILE);
        phone.addStyleName(ValoTheme.TEXTFIELD_ALIGN_CENTER);

email.setRequired(true);

        infoFields.addComponents(login, password,email);
        signFields.addComponents(lastname,firstname,sex,phone);
        contactFields.addComponents(cityCB,streetComboBox,streetStr,numberHouse);



        save.addClickListener((Button.ClickListener) event ->{

            try {
                cityCB.setValidationVisible(false);
                streetComboBox.setValidationVisible(false);
                lastname.setValidationVisible(false);
                firstname.setValidationVisible(false);
                email.setValidationVisible(false);

                          cityCB.validate();
                streetComboBox.validate();
                lastname.validate();
firstname.validate();
                email.validate();

            if(cityObject!=null){

                city = MyUI.getDataProvider().getCity(cityObject.getId());
                if(city==null) {
                    city = new City();

                    city.setIdCity(cityObject.getId());
                    city.setType(MyUI.getDataProvider().getType(cityObject.getType()));
                    city.setOkato(cityObject.getOkato());
                    city.setName(cityObject.getName());
                    city.setZip(cityObject.getZip());
                }
                List<KladrObject> parents = cityObject.getParents();
                if(parents!=null && parents.size()==1){
                    regionObject = parents.get(0);
                } else if(parents!=null && parents.size()==2) {
                    regionObject = parents.get(0);
                    districtObject = parents.get(1);
                    district = MyUI.getDataProvider().getDistrict(districtObject.getId());
                    if(district==null) {
                        district = new District();
                        district.setIdDistrict(districtObject.getId());
                        district.setOkato(districtObject.getOkato());
                        district.setZip(districtObject.getZip());
                        district.setName(districtObject.getName());
                        district.setType(MyUI.getDataProvider().getType(districtObject.getType()));
                    }
                }

                if(regionObject!=null){
                   region = MyUI.getDataProvider().getRegion(regionObject.getId());
                    if(region==null) {
                        region = new Region();
                        region.setIdRegion(regionObject.getId());
                        region.setName(regionObject.getName());
                        region.setZip(regionObject.getZip());
                        region.setOkato(regionObject.getOkato());
                        region.setType(MyUI.getDataProvider().getType(regionObject.getType()));
                    }
                }




            }

            if (streetObject!=null) {
                street = MyUI.getDataProvider().getStreet(streetObject.getId());
                if(street==null) {
                    street = new Street();
                    street.setIdStreet(streetObject.getId());
                    street.setName(streetObject.getName());
                    street.setZip(streetObject.getZip());
                    street.setOkato(streetObject.getOkato());

                street.setType(MyUI.getDataProvider().getType(streetObject.getType()));
                }
            }
                if(!numberHouse.isEmpty()){
                    house = MyUI.getDataProvider().getHouse(numberHouse.getValue());
                   if(house==null){
                       house = new House();
                        house.setNumber(numberHouse.getValue());
                   }
                }

                location = new Location();
                location.setCity(city);
                location.setStreet(street);
                location.setRegion(region);
                location.setDistrict(district);
                location.setHouse(house);



            DashboardEventBus.post(new DashboardEvent.UserSignupRequestedEvent(Byte.parseByte(String.valueOf(sex.getTabIndex())),location,login
                    .getValue(),phone.getValue(),email.getValue(),firstname.getValue(),lastname.getValue(), password.getValue()));

            } catch (Validator.InvalidValueException e) {
                cityCB.setValidationVisible(true);
                streetComboBox.setValidationVisible(true);
                lastname.setValidationVisible(true);
                firstname.setValidationVisible(true);
                email.setValidationVisible(true);

                Notification.show("Заполните все поля обязательные для ввода", Notification.Type.WARNING_MESSAGE);
            }


        });


        layoutInfo.addComponent(infoFields);
        layoutInfo.addComponent(signFields);
        layoutInfo.addComponent(contactFields);
        return layoutInfo;
    }



}
