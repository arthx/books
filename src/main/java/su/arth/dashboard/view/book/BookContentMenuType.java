package su.arth.dashboard.view.book;

import com.vaadin.navigator.View;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Resource;

/**
 * Created by arthur on 06.04.16.
 */
public enum BookContentMenuType {

    NEW_BOOK("Добавить книгу", FontAwesome.CLONE,true),
    EDIT_BOOK("Редактировать",FontAwesome.EDIT,true),
    REMOVE_BOOK("Удалить",FontAwesome.REMOVE,true);

    private final String viewName;
    private final Resource icon;
    private final boolean stateful;

    private BookContentMenuType(final String viewName,
                               final Resource icon,
                              final boolean stateful) {
        this.viewName = viewName;
        this.icon = icon;
        this.stateful = stateful;
    }

    public boolean isStateful() {
        return stateful;
    }

    public String getViewName() {
        return viewName;
    }


    public Resource getIcon() {
        return icon;
    }

    public static BookContentMenuType getByViewName(final String viewName) {
        BookContentMenuType result = null;
        for (BookContentMenuType viewType : values()) {
            if (viewType.getViewName().equals(viewName)) {
                result = viewType;
                break;
            }
        }
        return result;
    }
}
