package su.arth.dashboard.view.book;

/**
 * Created by salimhanov on 14.04.2016.
 */
public enum BookType {

    MYBOOKS(1,"Мои книги"), WISHBOOKS(0,"Желаемые книги");


   private int code;
   private String name;


    BookType(int code, String name) {
        this.code = code;
        this.name = name;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
