package su.arth.dashboard.view.users;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.themes.ValoTheme;
import su.arth.dashboard.event.DashboardEventBus;

/**
 * Created by salimhanov on 06.04.2016.
 */
public class UsersView extends TabSheet implements View,TabSheet.CloseHandler {


    public UsersView() {
        setSizeFull();
        addStyleName("users");
        addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);
        setCloseHandler(this);
        DashboardEventBus.register(this);

    }


    @Override
    public void onTabClose(TabSheet tabSheet, Component component) {

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
