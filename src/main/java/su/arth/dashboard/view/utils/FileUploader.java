package su.arth.dashboard.view.utils;

/**
 * Created by arthur on 03.04.16.
 */

import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.ui.*;
import su.arth.dashboard.domain.User;
import su.arth.dashboard.event.DashboardEvent;
import su.arth.dashboard.event.DashboardEventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class FileUploader extends Window {


    final Embedded image = new Embedded("Загрузка изображения");

    public FileUploader(User user){
/*
        ImageUploader receiver = new ImageUploader();
        image.setVisible(false);

        Upload upload = new Upload("Загрузить изображение", receiver);
        upload.setButtonCaption("Загрузить");
        upload.addSucceededListener(receiver);*/

    }

    public static void open(final User user) {
        DashboardEventBus.post(new DashboardEvent.CloseOpenWindowsEvent());
        Window w = new FileUploader(user);
        UI.getCurrent().addWindow(w);
        w.focus();
    }



}
