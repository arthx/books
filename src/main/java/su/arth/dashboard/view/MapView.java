package su.arth.dashboard.view;



import com.google.gwt.geolocation.client.Geolocation;
import com.google.gwt.geolocation.client.Position;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.tapio.googlemaps.GoogleMap;
import com.vaadin.tapio.googlemaps.client.LatLon;
import com.vaadin.tapio.googlemaps.client.overlays.GoogleMapMarker;
import com.vaadin.ui.Component;
import com.vaadin.ui.Panel;
import su.arth.dashboard.event.DashboardEventBus;
import su.arth.dashboard.service.location.LocationService;

/**
 * Created by arthur on 31.03.16.
 */
public class MapView extends Panel implements View{

    private static final String API_KEY = "AIzaSyDCi0YgFyGVl5QtZGKc_6NL8fCLXJCh1oY";
    private GoogleMap googleMap;

    private Geolocation geolocation;
    private Position.Coordinates coordinates;

    private LocationService locationService;

    public MapView(){
        locationService = new LocationService();
        setSizeFull();
        addStyleName("books");
        DashboardEventBus.register(this);
       setContent(createMap());




    }

public Component createMap() {
    googleMap = new GoogleMap(API_KEY, null, "russian");
    googleMap.setSizeFull();



buildMyLocation();

    googleMap.setZoom(15);

        return googleMap;

}
    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }


    private void buildMyLocation(){
        LatLon latlon = locationService.getMyLocation();
        googleMap.setCenter(latlon);
        GoogleMapMarker googleMapMarker = new GoogleMapMarker("Вы здесь", latlon, true);
        googleMapMarker.setDraggable(false);
        googleMap.addMarker(googleMapMarker);
    }


}
